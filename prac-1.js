//Task 1 
 
var radius = 4; 
 
var circumference = 2 * Math.PI * radius; 
 
var cAnswer= circumference.toFixed(2);
 
console.log(cAnswer); 


//Task 2 

let animalString = "cameldogcatlizard";
let andString = " and ";

console.log (animalString.substring(8,11) + andString + animalString.substring(5,8))


//Task 3

var personInfo ={
                  firstName: 'Kanye',
                  lastName: 'West',
                  birthDay: '8 June 1977',
                  annualIncome: 150000000
                 }

console.log (personInfo.firstName + personInfo.lastName + " was born on " + personInfo.birthDay + " and has an annual income of " + personInfo.annualIncome)

 
//Task 4

var number1, number2;


//RHS generates a random number between 1 and 10 inclusive

number1 = Math.floor((Math.random() * 10) + 1);

//RHS generates a random number between 1 and 10 inclusive

number2 = Math.floor((Math.random() * 10) + 1);

console.log("number1 = " + number1 + " number2 = " + number2);

//HERE your code to swap the values in number1 and number2
var tempN = 0;

tempN = number1;

number1 = number2;

number2 = tempN;

console.log("number1 = " + number1 + " number2 = " + number2);


//Task 5

let year;

let yearNot2015or2016;

year = 2000;

yearNot2015or2016 = year !== 2015 && year !== 2016;

console.log(yearNot2015or2016);