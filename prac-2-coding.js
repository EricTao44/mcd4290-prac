//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    
    let output = "" //empty output, fill this so that it can print onto the page.
    //Question 1 here 
    let data = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];

    let positiveOdd= [];
    let negativeEven= [];
    for (let i = 0; i <= data.length; i ++){
        if (data[i] < 0 && data[i] %2 == 0){
            negativeEven.push(data[i]);
        }
        else if (data[i] > 0 && data[i] %2 == 1){
            positiveOdd.push(data[i]);
        }
    }
    
    output += "Positive Odd : " + positiveOdd
   
    
    output += "\n Negative Even: " + negativeEven
   
   
    /*let output = "NegativeEven: ";
    
    //empty output, fill this so that it can print onto the page.
    
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];

    let negativeEvenArray = [];
    let positiveOddArray = [];
    
   
    
    for (let i = 0; i <= myArray.length; i++){
        if (myArray[i] < 0 && myArray[i] % 2 == 0){ 
            negativeEvenArray.push(myArray[i]);
        }
        else if (myArray[i] > 0 && myArray[i] % 2 == 1){ 
            positiveOddArray.push(myArray[i]);
        }
    }
    
  
    output += negativeEvenArray;
    
    
    output += "\n Positive Odd: ";
   
    
    output += positiveOddArray;
    
    */

    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}


function question2(){
    let output = "" 
    
    //Question 2 here 
    let one = 0
    let two = 0
    let three = 0
    let four = 0
    let five = 0
    let six = 0
    let tempNum = 0
    
    for (let i = 0; i <= 60000; i++){
        tempNum = Math.floor((Math.random() * 6) + 1)
        if (tempNum == 1){
            one ++ 
        }else if (tempNum == 2) {
            two ++
        }else if (tempNum == 3){
            three ++
        }else if (tempNum == 4){
            four ++
        }else if (tempNum == 5){
            five ++ 
        }else if (tempNum == 6){
            six ++
        }
    }
    output += "Frequence of die rolls"
    output += "\n1: " + one
    output += "\n2: " + two
    output += "\n3: " + three
    output += "\n4: " + four
    output += "\n5: " + five
    output += "\n6: " + six
    
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}


function question3(){
    let output = "" 
    
    //Question 3 here 
    let myArray = [0,0,0,0,0,0,0]
    let tempNum = 0
    
    for (let i = 0; i <= 60000; i++){
        tempNum = Math.floor((Math.random() * 6) + 1)
        myArray[tempNum] ++
    }
    output += "Frequence of die rolls"
    output += "\n1: " + myArray[1]
    output += "\n2: " + myArray[2]
    output += "\n3: " + myArray[3]
    output += "\n4: " + myArray[4]
    output += "\n5: " + myArray[5]
    output += "\n6: " + myArray[6]
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}


function question4(){
    let output = "" 
    
    //Question 4 here 
    let temp_num = 0 
	let exp_Val = 10000/100
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total: 60000,
        Exceptions: ""
    }
    for (i = 0; i <= dieRolls.Total; i++){
        temp_num = Math.floor((Math.random()*6)+1)
        if (temp_num == 1){
            dieRolls.Frequencies[1] ++
        }else if (temp_num == 2) {
            dieRolls.Frequencies[2] ++
        }else if (temp_num == 3){
            dieRolls.Frequencies[3] ++
        }else if (temp_num == 4){
            dieRolls.Frequencies[4] ++
        }else if (temp_num == 5){
            dieRolls.Frequencies[5] ++
        }else if (temp_num == 6){
            dieRolls.Frequencies[6] ++
        }
    }
	
	for (x = 0; x <= 6; x ++){
		if (dieRolls.Frequencies[x]- 10000 > exp_Val){
			dieRolls.Exceptions += x
		}
	}
	output += "Frequency of dice rolls\n"
	output += "Total rolls: " + dieRolls.Total + "\n"
    
	for(let prop in dieRolls.Frequencies){
        output += prop+':'+dieRolls.Frequencies[prop]+"\n"
        }
	output += "\nException : " +dieRolls.Exceptions
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}


function question5(){
    let output = "" 
    
    //Question 5 here 
    let tax = 0
    let person = {
        name : "Jane",
        income : 127050
    }
    
    if (person.income <= 18200 && person.income >0){
        tax = 0
        
    }else if (person.income > 18300 && person.income <= 37000){
        tax =(person.income - 18200) * 0.19 
        
    }else if (person.income > 37000 && person.income <= 90000){
        tax = 3572 + (percon.income - 37000) * 0.325
        
    }else if (person.income > 90000 && person.income <= 180000){
        tax = 20797 + (person.income - 90000) * 0.37
        
    }else if (person.income > 180000){
        tax = 54097 + (person.income -180000) * 0.45
    }else{
        tax = "invalid income amount"
    }
    
    output += person.name + "'s income is : " + person.income + ", and her tax owed is : $ " + tax
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}